import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { NavigationBarComponent } from './component/navigation-bar/navigation-bar.component';
import { StoryCardComponent } from './component/story-card/story-card.component';
import { NewsComponent } from './page/news/news.component';
import { BookmarkButtonComponent } from './component/bookmark-button/bookmark-button.component';
import { SpinLoaderComponent } from './component/spin-loader/spin-loader.component';
import { ArticleComponent } from './page/article/article.component';
import { HttpClientModule } from '@angular/common/http';
import { CustomTitleComponent } from './component/custom-title/custom-title.component';
import { SearchComponent } from './page/search/search.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    NavigationBarComponent,
    StoryCardComponent,
    NewsComponent,
    BookmarkButtonComponent,
    SpinLoaderComponent,
    ArticleComponent,
    CustomTitleComponent,
    SearchComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    ServiceWorkerModule.register('ngsw-worker.js', {
      enabled: environment.production,
    }),
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
