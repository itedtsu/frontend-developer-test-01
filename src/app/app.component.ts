import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  constructor() {}

  ngOnInit() {
    self.addEventListener('fetch', (fetchEvent: any) => {
      console.log('fetch !' + JSON.stringify(fetchEvent.request));
      return fetchEvent.respondWith(
        fetch(fetchEvent.request)
          .then((res) => {
            return res;
          })
          .catch(() => {
            //If there is an error, simply return offline page
            if (caches.match('/assets/offline-page.html')) {
              console.log('match found in cache for offline.html');
              return caches.match('/assets/offline-page.html');
            }
          })
      );
    });
  }
}
