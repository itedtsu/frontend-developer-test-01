import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router, RouterEvent } from '@angular/router';
import { BookmarkService } from 'src/app/service/bookmark/bookmark.service';
import { NewsService } from 'src/app/service/news/news.service';
import * as moment from 'moment';

interface News {
  headline: string;
  body: string;
  thumbnail: string;
  firstPublicationDate: string;
}

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class ArticleComponent implements OnInit {
  articleDetail: any;
  contents: News;
  isBookmark: boolean = false;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private newsService: NewsService,
    private bookmarkService: BookmarkService
  ) {
    this.router.events.subscribe((event: RouterEvent) => {
      this.articleDetail = JSON.parse(
        this.activatedRoute.snapshot.paramMap.get('article')
      );
    });
  }

  ngOnInit(): void {
    if (this.articleDetail) {
      this.getArticleDetailById(this.articleDetail.apiUrl);
      this.initialBookmark();
    }
  }

  initialBookmark() {
    this.isBookmark = this.bookmarkService.isArticleExist(this.articleDetail);
  }

  getArticleDetailById(apiUrl: string) {
    this.newsService.getNewsById(apiUrl).subscribe((res: any) => {
      if (res) {
        this.contents = res?.fields;
        this.contents.body = this.removeDate(this.contents.body);
        this.contents.body = this.addLineSection(this.contents.body);
      }
    });
  }

  removeDate(body) {
    let start = body.indexOf('<p class="block-time published-time">');
    let end = body.indexOf('</p>') + 4;
    let willRemove = body.slice(start, end);
    return body.replace(willRemove, '');
  }

  addLineSection(body) {
    let closeTitleTag = body.indexOf('</h>');
    let willReplace = body.slice(closeTitleTag, closeTitleTag + 4);
    return body.replace(willReplace, '</h><hr />');
  }

  onBookmarkArticle(booked) {
    this.bookmarkService.setBookmark(this.articleDetail);
    this.isBookmark = this.bookmarkService.isArticleExist(this.articleDetail);
  }

  changeDateFormat(date) {
    let dateMoment = moment(date);
    let newDateFormat =
      moment(date).format('dddd D MMM YYYY h.mm zz') +
      ' ' +
      String(dateMoment.local()).split(' ')[5];
    return newDateFormat;
  }
}
