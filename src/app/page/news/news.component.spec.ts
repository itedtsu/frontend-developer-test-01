import { HttpClientModule } from '@angular/common/http';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import {
  ComponentFixture,
  fakeAsync,
  TestBed,
  tick,
} from '@angular/core/testing';
import { BrowserModule } from '@angular/platform-browser';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';
import { NewsService } from 'src/app/service/news/news.service';

import { NewsComponent } from './news.component';

fdescribe('NewsComponent', () => {
  let component: NewsComponent;
  let fixture: ComponentFixture<NewsComponent>;
  const mockNewsService = {
    getNews: jasmine.createSpy('getNews').and.returnValue(
      of([
        {
          apiUrl: 'ApiUrl',
          fields: {
            bodyText: 'BodyText',
            headline: 'Headline',
            thumbnail: 'Thumbnail',
          },
          id: 'ID',
          isHosted: false,
          pillarId: 'pillar/news',
          pillarName: 'News',
          sectionId: 'technology',
          sectionName: 'Technology',
          type: 'liveblog',
          webPublicationDate: '2021-03-25T17:38:49Z',
          webTitle: 'WebTitle',
          webUrl: 'WebUrl',
        },
      ])
    ),
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [NewsComponent],
      imports: [BrowserModule, RouterTestingModule, HttpClientModule],
      providers: [{ provide: NewsService, useValue: mockNewsService }],
      schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NewsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call NewService getNew', fakeAsync(() => {
    let mockParams = {
      section: 'sport',
      page: 1,
      'page-size': 3,
    };
    let mockResponse = [
      {
        apiUrl: 'ApiUrl',
        fields: {
          bodyText: 'BodyText',
          headline: 'Headline',
          thumbnail: 'Thumbnail',
        },
        id: 'ID',
        isHosted: false,
        pillarId: 'pillar/news',
        pillarName: 'News',
        sectionId: 'technology',
        sectionName: 'Technology',
        type: 'liveblog',
        webPublicationDate: '2021-03-25T17:38:49Z',
        webTitle: 'WebTitle',
        webUrl: 'WebUrl',
      },
    ];
    component.getNews(mockParams, 'sport');
    tick();
    expect(mockNewsService.getNews).toHaveBeenCalledWith({
      section: 'sport',
      page: 1,
      'page-size': 3,
    });
    expect(JSON.stringify(component.sportNews)).toEqual(
      JSON.stringify(mockResponse)
    );
  }));
});
