import { Component, OnInit } from '@angular/core';
import { LoaderService } from 'src/app/service/loader/loader.service';
import { NewsService } from 'src/app/service/news/news.service';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.scss'],
})
export class NewsComponent implements OnInit {
  hilightNews = [];
  sportNews = [];
  latestNews = [];
  defaultParams = {
    page: 1,
    'page-size': 9,
  };

  constructor(
    private newsService: NewsService,
    private loaderService: LoaderService
  ) {}

  ngOnInit(): void {
    this.loaderService.show();
    this.getHilightNews();
    this.getSportNews();
    this.getLatestNews();
  }

  onOrderByChange(order) {
    this.getHilightNews({ 'order-by': order });
    this.getSportNews({ 'order-by': order });
    this.getLatestNews({ 'order-by': order });
  }

  getNews(params, type, clear?) {
    this.loaderService.show();
    this.newsService.getNews(params).subscribe(
      (news) => {
        if (news) {
          if (type == 'hilight') this.hilightNews = news;
          if (type == 'sport') this.sportNews = news;
          if (type == 'latest') {
            this.defaultParams.page++;
            this.latestNews = clear ? news : [...this.latestNews, ...news];
            if (clear) this.defaultParams.page = 1;
          }
        }
        this.loaderService.hide();
      },
      (error) => {
        this.loaderService.hide();
      }
    );
  }

  getHilightNews(order?) {
    let initialHilight = {
      section: 'technology',
      page: 1,
      'page-size': 8,
    };
    if (order) initialHilight = { ...initialHilight, ...order };
    this.getNews(initialHilight, 'hilight', !!order);
  }

  getSportNews(order?) {
    let initialSport = {
      section: 'sport',
      page: 1,
      'page-size': 3,
    };
    if (order) initialSport = { ...initialSport, ...order };
    this.getNews(initialSport, 'sport', !!order);
  }

  getLatestNews(order?) {
    if (order) this.defaultParams = { ...this.defaultParams, ...order };
    this.getNews(this.defaultParams, 'latest', !!order);
  }
}
