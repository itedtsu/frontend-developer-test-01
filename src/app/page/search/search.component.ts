import { Component, OnInit } from '@angular/core';
import {
  ActivatedRoute,
  NavigationEnd,
  Router,
  RouterEvent,
} from '@angular/router';
import { BookmarkService } from 'src/app/service/bookmark/bookmark.service';
import { LoaderService } from 'src/app/service/loader/loader.service';
import { NewsService } from 'src/app/service/news/news.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss'],
})
export class SearchComponent implements OnInit {
  title: string = '';
  isShowBookmark: boolean = false;
  news = [];
  defaultParams = {
    'current-page': 1,
    page: 1,
    'page-size': 9,
  };

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private newsService: NewsService,
    private loaderService: LoaderService,
    private bookmarkService: BookmarkService
  ) {
    this.router.events.subscribe((event: RouterEvent) => {
      if (event instanceof NavigationEnd) {
        if (event.url == '/bookmark') {
          this.title = 'All bookmark';
          this.news = this.bookmarkService.getBookmark();
          this.news.sort((a, b) => {
            let c: any = new Date(a.webPublicationDate);
            let d: any = new Date(b.webPublicationDate);
            return d - c;
          });
        } else if (event.url && event.url.includes('/search-result')) {
          this.isShowBookmark = true;
          let searchParams = JSON.parse(
            this.activatedRoute.snapshot.paramMap.get('search')
          );
          this.title = `Search result \"${searchParams.q}\"`;

          this.search(searchParams);
        }
      }
    });
  }

  ngOnInit(): void {}

  search(params?, clear?) {
    this.loaderService.show();
    if (params) this.defaultParams = { ...this.defaultParams, ...params };
    this.newsService.getNews(this.defaultParams).subscribe((news) => {
      this.defaultParams.page++;
      this.news = clear ? news : [...this.news, ...news];
      if (clear) this.defaultParams.page = 1;
      this.loaderService.hide();
    });
  }

  onOrderByChanged(order) {
    if (this.title === 'All bookmark') {
      this.news.sort((a, b) => {
        let c: any = new Date(a.webPublicationDate);
        let d: any = new Date(b.webPublicationDate);
        return order == 'newest' ? d - c : c - d;
      });
    } else {
      this.search({ 'order-by': order }, true);
    }
  }
}
