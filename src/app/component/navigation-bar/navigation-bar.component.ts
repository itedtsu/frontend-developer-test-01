import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { NewsService } from 'src/app/service/news/news.service';

@Component({
  selector: 'app-navigation-bar',
  templateUrl: './navigation-bar.component.html',
  styleUrls: ['./navigation-bar.component.scss'],
})
export class NavigationBarComponent implements OnInit {
  isSearch: boolean = false;
  constructor(private router: Router) {}

  ngOnInit(): void {}

  search(val) {
    if (val) {
      let params = {
        q: val,
      };
      this.router.navigate([
        '/search-result',
        { search: JSON.stringify(params) },
      ]);
    }
  }
}
