import {
  Component,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
  ViewEncapsulation,
} from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-story-card',
  templateUrl: './story-card.component.html',
  styleUrls: ['./story-card.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class StoryCardComponent implements OnInit, OnChanges {
  @Input('cardType') cardType: string = 'Large';
  @Input('articleDetail') articleDetail;
  contents: any;
  description: string;

  constructor(private router: Router) {}

  ngOnInit(): void {}

  ngOnChanges(changes: SimpleChanges) {
    if (changes.articleDetail) {
      this.contents = this.articleDetail?.fields;
      this.description = this.wrapDescripiton(this.contents?.bodyText);
    }
  }

  onClickStoryCard(id) {
    this.router.navigate([
      '/article/' + id,
      { article: JSON.stringify(this.articleDetail) },
    ]);
  }

  wrapDescripiton(body: string) {
    if (body) return body.substring(0, 100) + '&hellip;';
    return body;
  }
}
