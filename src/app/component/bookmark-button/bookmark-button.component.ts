import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-bookmark-button',
  templateUrl: './bookmark-button.component.html',
  styleUrls: ['./bookmark-button.component.scss'],
})
export class BookmarkButtonComponent implements OnInit {
  @Input() isBookmark: boolean;
  @Input() title: string = 'View Bookmark';
  @Output() willClick = new EventEmitter();

  constructor() {}

  ngOnInit(): void {}

  onClickFunction() {
    this.willClick.emit(!this.isBookmark);
  }
}
