import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-custom-title',
  templateUrl: './custom-title.component.html',
  styleUrls: ['./custom-title.component.scss'],
})
export class CustomTitleComponent implements OnInit {
  @Input() title: string;
  @Input() isShowFilter: boolean;
  @Input() isShowBookmark: boolean;
  @Input() isShowViewMore: boolean;
  @Output() sortChange = new EventEmitter<string>();
  orderBy: string = 'newest';

  constructor(private router: Router) {}

  ngOnInit(): void {}

  onOrderByChange(e) {
    this.sortChange.emit(e.target.value);
  }

  onViewMore(type: string) {
    let params = {
      q: type.toLowerCase(),
    };

    this.router.navigate([
      '/search-result',
      { search: JSON.stringify(params) },
    ]);
  }
}
