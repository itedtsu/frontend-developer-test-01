import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class BookmarkService {
  constructor() {}

  public getBookmark() {
    let bookmark = localStorage.getItem('bookmark');
    return bookmark ? JSON.parse(bookmark) : [];
  }

  public setBookmark(article: any) {
    let bookmarks = this.getBookmark();

    let alreadyBooked = this.isExist(bookmarks, article);

    if (alreadyBooked) {
      bookmarks = bookmarks.filter((res) => res.id !== article.id);
    } else {
      bookmarks.push(article);
    }
    localStorage.setItem('bookmark', JSON.stringify(bookmarks));
  }

  isExist(bookmarks: Array<string>, article) {
    let isAlreadyBook = bookmarks.find((res: any) => {
      return res.id == article.id;
    });
    return !!isAlreadyBook;
  }

  public isArticleExist(article) {
    return this.isExist(this.getBookmark(), article);
  }
}
