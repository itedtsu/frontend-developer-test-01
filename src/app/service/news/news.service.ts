import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class NewsService {
  constructor(private http: HttpClient) {}

  getNews(filters?) {
    let params: any = {
      'api-key': '7bc6f97b-7316-42ec-b3bd-e98abee4c98f',
      'order-by': 'newest',
      lang: 'en',
      'show-fields': 'headline,thumbnail,bodyText',
    };
    if (filters) params = { ...params, ...filters };
    return this.http
      .get<any>(`https://content.guardianapis.com/search`, { params: params })
      .pipe(
        map((news) => {
          return news.response.results;
        })
      );
  }

  getNewsById(apiUrl) {
    let params: any = {
      'api-key': '7bc6f97b-7316-42ec-b3bd-e98abee4c98f',
      'show-fields': 'headline,body,thumbnail,firstPublicationDate',
    };
    return this.http.get(apiUrl, { params: params }).pipe(
      map((news: any) => {
        return news.response.content;
      })
    );
  }
}
