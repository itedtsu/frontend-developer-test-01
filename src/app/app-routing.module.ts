import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ArticleComponent } from './page/article/article.component';
import { NewsComponent } from './page/news/news.component';
import { SearchComponent } from './page/search/search.component';

const routes: Routes = [
  { path: 'news', pathMatch: 'full', component: NewsComponent },
  { path: 'search-result', pathMatch: 'full', component: SearchComponent },
  { path: 'bookmark', pathMatch: 'full', component: SearchComponent },
  {
    path: 'article/:id',
    children: [
      {
        path: '**',
        component: ArticleComponent,
      },
    ],
  },
  { path: '', redirectTo: '/news', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { onSameUrlNavigation: 'reload' })],
  exports: [RouterModule],
})
export class AppRoutingModule {}
